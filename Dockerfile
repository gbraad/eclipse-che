FROM registry.gitlab.com/gbraad/fedora:24
MAINTAINER Gerard Braad <me@gbraad.nl

ENV LANG=C.UTF-8 \
    JAVA_HOME=/usr/lib/jvm/jre \
    PATH=${PATH}:${JAVA_HOME}/bin \
    CHE_HOME=/home/user/che

RUN dnf -y install sudo java-1.8.0-openjdk && \
    dnf clean all && \
    adduser user && \
    echo "user ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user && \
    chmod 0440 /etc/sudoers.d/user && \
    mkdir -p /workspace && \
    chown user:user /workspace && \
    mkdir -p /home/user/che && \
    chown user /home/user/che && \
    ln -s /workspace /home/user/che/workspaces && \
    chown user /home/user/che/workspaces

VOLUME /workspace
WORKDIR /workspace

EXPOSE 8000 8080

USER user

ADD che/assembly/assembly-main/target/eclipse-che-*/eclipse-che-* /home/user/che/

ENTRYPOINT [ "/home/user/che/bin/che.sh", "-c" ]